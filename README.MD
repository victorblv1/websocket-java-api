## Primitive WebSocket Server

Given websocket is written with Java API for websockets.

Since the give websocket server has its own embedded tomcat9 container
It can be run stand alone. All we need:

build gradle app:
``gradle build``<br>
run the app:
``gradle appRun``<br>
click the following [link](http://localhost:8080)

Make sure you have JDK installed (i've used adoptopenjdk-11).
